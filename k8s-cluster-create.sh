#!/bin/bash
set -xe

# create K8S cluster
gcloud beta container --project "halogen-base-223914" clusters create "standard-cluster-1" --zone "europe-west4-a" --username "admin" --cluster-version "1.11.2-gke.18" --machine-type "g1-small" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "3" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/halogen-base-223914/global/networks/default" --subnetwork "projects/halogen-base-223914/regions/europe-west4/subnetworks/default" --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair
INSTANCE_GROUP_NAME=$(gcloud compute instance-groups list --format=json | jq -r .[].name)
K8S_STATUS=$(gcloud container clusters list --format=json | jq -r .[].status)

while [ "$K8S_STATUS" != "RUNNING" ];do 
  echo "CLUSTER NOT READY"
  sleep 30
done

# Deploy go-app as a service in k8s
kubectl apply -f go-app-deployment.yaml
kubectl apply -f go-app-service.yaml

# set up GCP loadblancer for instance group, in case of autoscaling
gcloud compute http-health-checks create my-healthcheck --host app.ibejzik.ml --port 8080 --request-path=/
gcloud compute instance-groups set-named-ports my-instance-group --named-ports 8080:8080
gcloud compute backend-services create my-http-backend-service --http-health-checks my-healthcheck --port-name 8080 --protocol HTTP
gcloud compute backend-services add-backend my-http-backend-service --instance-group "$INSTANCE_GROUP_NAME" --balancing-mode RATE --max-rate-per-instance 10
gcloud compute url-maps create my-url-map --default-service my-http-backend-service
gcloud compute addresses create nginx-ssl --global
LB_IP=$(gcloud compute addresses list --format=json | jq -r .[].address)
gcloud compute forwarding-rules create my-http-forwarding-rule --global --address "$LB_IP" --ip-protocol TCP --port-range 80 --target-http-proxy my-http-proxy
# Deploy nginx with let encrypt for SSL termination and point it to LB_IP
kubectl apply -f nginx-ssl-deployment.yaml
kubectl apply -f nginx-ssl-deployment.yaml