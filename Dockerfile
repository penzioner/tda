FROM golang:1.11.2-alpine3.8 AS build

# build executable
WORKDIR /go/src/app
COPY outyet/ .
RUN go build
RUN ls -lha

# copy executable
FROM golang:1.11.2-alpine3.8
COPY --from=build /go/src/app/app /go/src/app/
WORKDIR /go/src/app

CMD ["./app"]
