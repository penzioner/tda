# tda

As instructed in the github chalenge description, I created a go app in kubernetes in google cloud.
1. Code is stored in gitlab because they have their own CI system (gitlab-ci) which I had some experience.
2. Docker image is automatically built and pushed to dockerhub on commit.
3. Google cloud is selected because of ease of setup for kubernetes cluster (compared for example AWS EKS).
   This is also important from automation point.
   K8S cluster consists of 3 nodes which are in a instance group that can be autoscaled
   Because of autoscaling I have setup a L7 load balancer to target that instance group.
   SSL termination is done on a separate instance where I installed nginx as a SSL termination that proxies to the load-balancer
   Certificate is setup to be renewed by cron job every 2 months.
   Improvement of that would be putting the nginx and certbot in a docker image and also deploy it in kuberentes. Certificate renewal can be by running the certbot tool to update the image.
4. Go app is deployed in kubernetes cluster and exposed to loadbalancer on non standard port.
5. For Deleting and creating resources I used gcloud cli tool.
   Deleting of resources can be done with deleting the project in GCP
   For provisioning  I wrote a quick and dirty shell script, in the future I would have tried to move that logic to Terraform.
       